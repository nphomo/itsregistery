# itsregistery



# itsregistry

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/nphomo/itsregistery.git
git branch -M main
git push -uf origin main
```
## A git branch should start with a category. Pick one of these: feature, bugfix, hotfix, or test.

**feature** is for adding, refactoring or removing a feature  
**bugfix** is for fixing a bug  
**hotfix** is for changing code with a temporary solution and/or without following the usual process (usually because of an emergency)  
**test** is for experimenting outside of an issue/ticket  

### Example
- [ ]You need to add, refactor or remove a feature: **git branch feature/issue-42/create-new-button-component**  
- [ ]You need to fix a bug: **git branch bugfix/issue-342/button-overlap-form-on-mobile**  
- [ ]You need to fix a bug really fast (possibly with a temporary solution): **git branch hotfix/no-ref/- [registration-form-not-working**  
- [ ]You need to experiment outside of an issue/ticket: **git branch test/no-ref/refactor-components-with-atomic-design**

## Commit Naming Category

**feat** is for adding a new feature  
**fix** is for fixing a bug  
**refactor** is for changing code for peformance or convenience purpose (e.g. readibility)  
**chore** is for everything else (writing documentation, formatting, adding tests, cleaning useless code etc.)  
After the category, there should be a ":" announcing the commit description.

Statement(s)

git commit -m '<category: do something; do some other things>'
### Examples:

```git commit -m 'feat: add new button component; add new button components to templates'```  
```git commit -m 'fix: add the stop directive to button component to prevent propagation'```  
```git commit -m 'refactor: rewrite button component in TypeScript'```  
```git commit -m 'chore: write button documentation'```
